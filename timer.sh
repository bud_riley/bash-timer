#! /bin/bash

read -p 'How many min? ' minvar

read -p 'How man sec? ' secvar

if [ -z $minvar ]
then
minvar=0
fi

if [ -z $secvar ]
then
secvar=0
fi

totvar=$(($minvar * 60))

totvar=$(($totvar + $secvar))

SECONDS=0

kepp=5

while [ $SECONDS -lt $totvar ]
    do
    totsec=$(($totvar - $SECONDS))
    countsec=$(($totsec % 60))
    countmin=$((($totsec - $countsec) / 60))
    printf "\rYou have $countmin minutes and $countsec seconds left on your glorious timer!"
    sleep 5
done

printf '\n%s\n' 'Your glorious timer is done!'

<<COMMENT  #Uncomment if you would like to use "pactl" a pulse audio tool instead of sox,
#however, if you cancel during its play, pactl will keep playing its tone until canceled with "pactl unload-module module-sine"

freqvar=500
jarvar=0
freqfvar=1000

while [ $jarvar -lt 5 ]
do
freqvar=$(($freqvar + 500))
pactl load-module module-sine frequency=$freqvar >/dev/null 2>&1
freqqvar=$(($freqvar + 20))
pactl load-module module-sine frequency=$freqqvar >/dev/null 2>&1
sleep 0.4
pactl unload-module module-sine
freqfvar=$(($freqfvar + 500))
pactl load-module module-sine frequency=$freqfvar >/dev/null 2>&1
freqqfvar=$(($freqvar + 20))
pactl load-module module-sine frequency=$freqqfvar >/dev/null 2>&1
sleep 0.2
pactl unload-module module-sine
sleep 0.1
jarvar=$(($jarvar + 1))
done

jarvar=0

while [ $jarvar -lt 5 ]
do
freqvar=$(($freqvar - 500))
pactl load-module module-sine frequency=$freqvar >/dev/null 2>&1
freqqvar=$(($freqvar + 20))
pactl load-module module-sine frequency=$freqqvar >/dev/null 2>&1
sleep 0.2
pactl unload-module module-sine
sleep 0.1
jarvar=$(($jarvar + 1))
done

COMMENT

#<<COMMENTDUDE

freqvar=500
jarvar=0

while [ $jarvar -lt 5 ]
do
freqvar=$(($freqvar + 500))
freqqvar=$(($freqvar + 50))
play -n synth sin $freqvar sin $freqqvar fade q 0.01 0.3 0.01 >/dev/null 2>&1
jarvar=$(($jarvar + 1))
done

jarvar=0

freqvar=2000
while [ $jarvar -lt 3 ]
do
freqvar=$(($freqvar + 700))
freqqvar=$(($freqvar + 50))
play -n synth sin $freqvar sin $freqqvar fade q 0.01 0.3 0.01 >/dev/null 2>&1
jarvar=$(($jarvar + 1))
done

jarvar=0
freqvar=3000

while [ $jarvar -lt 4 ]
do
freqvar=$(($freqvar - 500))
freqqvar=$(($freqvar + 50))
play -n synth sin $freqvar sin $freqqvar fade q 0.01 0.3 0.01 >/dev/null 2>&1
jarvar=$(($jarvar + 1))
done

#COMMENTDUDE


#if you have a file you would like to use instead
#aplay /path/to/your/file.wav
