This is a simple bash timer.

it prompts for minutes then seconds, counts down in 5-second increments and 
then sounds an alarm.

To work as intended, you will need sox -
https://packages.debian.org/unstable/sound/sox

If you do not have sox and still want an audio alarm at the end of the timer,
you must uncomment the pactl section and comment or remove the sox section.